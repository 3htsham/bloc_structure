import 'package:bloc/bloc.dart';

import '../../configs/configs.dart';
import '../../utils/preferences.dart';
import 'bloc.dart';

class LanguageBloc extends Bloc<LanguageEvent, LanguageState> {
  LanguageBloc(LanguageState initialState) : super(initialState) {
    on<ChangeLanguage>((event, emit) async {
      if (event.locale == AppLanguage.defaultLanguage) {
        emit(LanguageUpdated());
      } else {
        emit(LanguageUpdating());
        AppLanguage.defaultLanguage = event.locale;
        await UtilPreferences.setString(
            Preferences.language, event.locale.languageCode);
        emit(LanguageUpdated());
      }
    });
  }
}
