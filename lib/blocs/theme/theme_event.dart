import 'package:meta/meta.dart';

import '../../enums/enums.dart';
import '../../models/models.dart';

@immutable
abstract class ThemeEvent {}

class ChangeTheme extends ThemeEvent {
  final ThemeModel? theme;
  final String? font;
  final DarkOption? darkOption;

  ChangeTheme(
      {required this.theme, required this.darkOption, required this.font});
}

class ChangeDarkOption extends ThemeEvent {
  final DarkOption darkOption;

  ChangeDarkOption(this.darkOption);
}
