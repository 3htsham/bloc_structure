import 'package:bloc/bloc.dart';

import '../../configs/configs.dart';
import '../../enums/enums.dart';
import '../../utils/preferences.dart';
import 'bloc.dart';

const darkDynamic = 'dynamic';
const darkAlwaysOff = 'off';
const darkAlwaysOn = 'on';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc(ThemeState initialState) : super(initialState) {
    on<ChangeTheme>((event, emit) {
      emit(ThemeUpdating());

      AppTheme.currentTheme = event.theme ?? AppTheme.currentTheme;
      AppTheme.currentFont = event.font ?? AppTheme.currentFont;
      AppTheme.darkThemeOption = event.darkOption ?? AppTheme.darkThemeOption;

      ///Setup theme with setting darkOption
      switch (AppTheme.darkThemeOption) {
        case DarkOption.alwaysOn:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.darkTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
        case DarkOption.alwaysOff:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
        case DarkOption.dynamic:
        default:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.darkTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
      }

      ///Save in utils
      UtilPreferences.setString(Preferences.theme, AppTheme.currentTheme.name);
      UtilPreferences.setString(Preferences.font, AppTheme.currentFont);
      switch (AppTheme.darkThemeOption) {
        case DarkOption.alwaysOn:
          UtilPreferences.setString(Preferences.darkOption, darkAlwaysOn);
          break;
        case DarkOption.alwaysOff:
          UtilPreferences.setString(Preferences.darkOption, darkAlwaysOff);
          break;
        case DarkOption.dynamic:
        default:
          UtilPreferences.setString(Preferences.darkOption, darkDynamic);
          break;
      }

      emit(ThemeUpdated());
    });

    on<ChangeDarkOption>((event, emit) {
      emit(ThemeUpdating());
      AppTheme.darkThemeOption = event.darkOption;

      ///Setup theme with setting darkOption
      switch (AppTheme.darkThemeOption) {
        case DarkOption.alwaysOn:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.darkTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
        case DarkOption.alwaysOff:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
        case DarkOption.dynamic:
        default:
          AppTheme.darkTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.darkTheme,
            font: AppTheme.currentFont,
          );
          AppTheme.lightTheme = CollectionTheme.getCollectionTheme(
            theme: AppTheme.currentTheme.lightTheme,
            font: AppTheme.currentFont,
          );
          break;
      }

      ///Save in Utils
      switch (AppTheme.darkThemeOption) {
        case DarkOption.alwaysOn:
          UtilPreferences.setString(Preferences.darkOption, darkAlwaysOn);
          break;
        case DarkOption.alwaysOff:
          UtilPreferences.setString(Preferences.darkOption, darkAlwaysOff);
          break;
        case DarkOption.dynamic:
        default:
          UtilPreferences.setString(Preferences.darkOption, darkDynamic);
          break;
      }
      emit(ThemeUpdated());
    });
  }
}
