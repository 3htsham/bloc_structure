import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../configs/configs.dart';
import '../../enums/enums.dart';
import '../../models/models.dart';
import '../../utils/utils.dart';
import '../blocs.dart';

class ApplicationBloc extends Bloc<ApplicationEvent, ApplicationState> {
  final ThemeBloc? themeBloc;
  final LanguageBloc? langBloc;

  // final AuthBloc authBloc;

  ApplicationBloc(
    ApplicationState initialState, {
    required this.langBloc,
    required this.themeBloc,
    // @required this.authBloc
  }) : super(initialState) {
    on<SetupApplication>((event, emit) async {
      emit(ApplicationWaiting());
      Application.prefs = await SharedPreferences.getInstance();

      ThemeModel? theme;
      String? font;
      DarkOption? darkOption;

      ///Get old theme and language settings
      final oldLanguage = UtilPreferences.getString(Preferences.language);
      final oldFont = UtilPreferences.getString(Preferences.font);
      final oldTheme = UtilPreferences.getString(Preferences.theme);
      final oldDarkOption = UtilPreferences.getString(Preferences.darkOption);

      ///Setup language
      if (oldLanguage != null) {
        langBloc!.add(ChangeLanguage(Locale(oldLanguage)));
      }

      ///Find if font support available
      final fontAvailable =
          AppTheme.fontSupport.where((element) => element == oldFont).toList();

      ///Find if theme support available
      final themeAvailable = AppTheme.themeSupport
          .where((element) => element.name == oldTheme)
          .toList();
      if (fontAvailable.isNotEmpty) {
        font = fontAvailable.first;
      }
      if (themeAvailable.isNotEmpty) {
        theme = themeAvailable.first;
      }
      if (oldDarkOption != null) {
        switch (oldDarkOption) {
          case darkAlwaysOn:
            darkOption = DarkOption.alwaysOn;
            break;
          case darkAlwaysOff:
            darkOption = DarkOption.alwaysOff;
            break;
          case darkDynamic:
          default:
            darkOption = DarkOption.dynamic;
            break;
        }
      }

      ///Setup theme and font with dark optionn
      themeBloc!
          .add(ChangeTheme(theme: theme!, darkOption: darkOption, font: font));
      //TODO: Have to check Authentication as well
      ///Authentication begin check
      // authBloc.add(AuthenticationCheck());
      ///First or After upgrade version show intro preview app
      bool hasReview = UtilPreferences.containsKey(
          '${Application.version}.${Preferences.reviewIntro}');
      if (hasReview) {
        emit(ApplicationSetupCompleted());
      } else {
        emit(ApplicationIntroView());
      }
    });

    on<OnCompleteIntro>((event, emit) async {
      await UtilPreferences.setBool(
          '${Application.version}.${Preferences.reviewIntro}', true);
      emit(ApplicationSetupCompleted());
    });
  }
}
