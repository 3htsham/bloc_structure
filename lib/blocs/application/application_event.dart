import 'package:meta/meta.dart';

@immutable
abstract class ApplicationEvent {}

class SetupApplication extends ApplicationEvent {}

class OnCompleteIntro extends ApplicationEvent {}
