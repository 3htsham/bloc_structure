import 'package:flutter/cupertino.dart';

import '../configs/configs.dart';
import '../utils/utils.dart';

class AppLocaleDelegate extends LocalizationsDelegate<Translate> {
  const AppLocaleDelegate();

  @override
  bool isSupported(Locale locale) =>
      AppLanguage.supportedLocales.contains(locale);

  @override
  Future<Translate> load(Locale locale) async {
    Translate localizations = Translate(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<Translate> old) => false;
}
