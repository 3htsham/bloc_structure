import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import '../blocs/blocs.dart';
import '../configs/configs.dart';
import '../routes/routes.dart';
import '../utils/translate.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  late ApplicationBloc _applicationBloc;
  late LanguageBloc _languageBloc;
  late ThemeBloc _themeBloc;

  // late AuthBloc _authBloc;

  @override
  void initState() {
    _languageBloc = LanguageBloc(InitialLanguageState());
    _themeBloc = ThemeBloc(InitialThemeState());
    _applicationBloc = ApplicationBloc(
      InitialApplicationState(),
      langBloc: _languageBloc, themeBloc: _themeBloc,
      // authBloc: _authBloc
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ApplicationBloc>(create: (context) => _applicationBloc),
        BlocProvider<LanguageBloc>(create: (context) => _languageBloc),
        BlocProvider<ThemeBloc>(create: (context) => _themeBloc),
      ],
      child: BlocBuilder<LanguageBloc, LanguageState>(
        builder: (ctx, LanguageState _state) {
          return BlocBuilder<ThemeBloc, ThemeState>(
            builder: (_ctx, ThemeState _themeState) {
              return MaterialApp(
                title: Application.appName,
                debugShowCheckedModeBanner: Application.debug,
                theme: AppTheme.lightTheme,
                darkTheme: AppTheme.darkTheme,
                onGenerateRoute: RouteGenerator.onGenerateRoute,
                locale: AppLanguage.defaultLanguage,
                localizationsDelegates: const [
                  Translate.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate
                ],
                supportedLocales: AppLanguage.supportedLocales,
                home: BlocBuilder<ApplicationBloc, ApplicationState>(
                  builder: (_context, ApplicationState appState) {
                    if (appState is Container) {
                      //TODO: return Home Screen
                    }
                    if (appState is ApplicationIntroView) {
                      //TODO: return Intro Screen
                      return Container();
                    }
                    //TODO: return SplashScreen
                    return Container();
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
