import 'package:flutter/material.dart';

class ThemeModel {
  final String name;
  final Color color;
  final String lightTheme;
  final String darkTheme;

  ThemeModel(this.name, this.color, this.darkTheme, this.lightTheme);

  factory ThemeModel.fromJson(Map<String, dynamic> json) {
    return ThemeModel(json['name'] ?? 'Unknown', json['color'] ?? Colors.black,
        json['dark'] ?? 'Unknown', json['light'] ?? 'Unknown');
  }
}
