import 'package:flutter/material.dart';

import 'configs.dart';

class CollectionTheme {
  static ThemeData getCollectionTheme(
      {String theme = 'primaryLight', String font = AppTheme.defaultFont}) {
    switch (theme) {
      case 'primaryLight':
      default:
        const _halfWhiteColor = Color(0xFFf6f6f6);
        const _primaryColorDark = Color(0xFF0096c7);
        const _accentColor = Color(0xff0093e6);
        const _scaffoldColor = Colors.white;
        const _textColor = Color(0xFF7a8aa3);
        const _disabledColor = Color(0xFF9999aa);
        const _errorColor = Color(0xffd32f2f);
        //Color(0xff0093e6);
        const swatch = MaterialColor(4293223245, {
          50: Color.fromRGBO(0, 147, 230, .1),
          100: Color.fromRGBO(0, 147, 230, .2),
          200: Color.fromRGBO(0, 147, 230, .3),
          300: Color.fromRGBO(0, 147, 230, .4),
          400: Color.fromRGBO(0, 147, 230, .5),
          500: Color.fromRGBO(0, 147, 230, .6),
          600: Color.fromRGBO(0, 147, 230, .7),
          700: Color.fromRGBO(0, 147, 230, .8),
          800: Color.fromRGBO(0, 147, 230, .9),
          900: Color.fromRGBO(0, 147, 230, 1),
        });
        return ThemeData(
          primarySwatch: swatch,
          fontFamily: font,
          brightness: Brightness.light,
          primaryColor: _halfWhiteColor,
          scaffoldBackgroundColor: _scaffoldColor,
          // primaryColorLight: const Color(0xffFF8A65),
          primaryColorDark: _primaryColorDark,
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: swatch,
            accentColor: _accentColor,
            cardColor: _halfWhiteColor,
            brightness: Brightness.light,
          ),
          disabledColor: _disabledColor,
          focusColor: _disabledColor,
          // canvasColor: const Color(0xfffafafa),
          // bottomAppBarColor: const Color(0xffffffff),
          // cardColor: const Color(0xffffffff),
          // dividerColor: const Color(0x1f000000),
          // highlightColor: const Color(0x66bcbcbc),
          // splashColor: const Color(0x66c8c8c8),
          // selectedRowColor: const Color(0xfff5f5f5),
          // unselectedWidgetColor: const Color(0x8a000000),
          // buttonColor: const Color(0xffe5634d),
          // toggleableActiveColor: const Color(0xff4A90A4),
          // secondaryHeaderColor: const Color(0xfffcebe9),
          // textSelectionColor: const Color(0xff4A90A4),
          // cursorColor: const Color(0xff4285f4),
          // textSelectionHandleColor: const Color(0xff4A90A4),
          backgroundColor: _scaffoldColor,
          dialogBackgroundColor: _scaffoldColor,
          indicatorColor: _accentColor,
          hintColor: _textColor.withOpacity(0.7),
          errorColor: _errorColor,
          textSelectionTheme: const TextSelectionThemeData(
              selectionColor: _accentColor,
              cursorColor: _primaryColorDark,
              selectionHandleColor: _accentColor),
          buttonTheme: const ButtonThemeData(
            textTheme: ButtonTextTheme.accent,
            height: 46,
            // padding: EdgeInsets.only(left: 16, right: 16),
            // shape: RoundedRectangleBorder(
            //   side: BorderSide(
            //     color: Color(0xff000000),
            //     width: 0,
            //     style: BorderStyle.none,
            //   ),
            //   borderRadius: BorderRadius.all(
            //     Radius.circular(8),
            //   ),
            // ),
            buttonColor: _accentColor,
            // disabledColor: Color(0x61000000),
            // highlightColor: Color(0x29000000),
            // splashColor: Color(0x1f000000),
            // focusColor: Color(0x1f000000),
            // hoverColor: Color(0x0a000000),
          ),
          inputDecorationTheme: InputDecorationTheme(
            contentPadding: const EdgeInsets.only(
              top: 12,
              bottom: 12,
              left: 15,
              right: 15,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: _disabledColor, width: 1)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: _disabledColor, width: 1)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: _disabledColor, width: 1)),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                    color: _disabledColor.withOpacity(0.7), width: 1)),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: _errorColor, width: 1),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: _errorColor, width: 1),
            ),
          ),
          /*
          chipTheme: ChipThemeData(
            backgroundColor: const Color(0x1f000000),
            brightness: Brightness.light,
            deleteIconColor: const Color(0xffdf3c20),
            disabledColor: const Color(0x0c000000),
            labelPadding: const EdgeInsets.only(left: 8, right: 8),
            labelStyle: TextStyle(
              fontSize: 12,
              fontFamily: font,
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
            padding: const EdgeInsets.all(4),
            secondaryLabelStyle: TextStyle(
              fontSize: 12,
              fontFamily: font,
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
            secondarySelectedColor: const Color(0x3de5634d),
            selectedColor: const Color(0x3de5634d),
            shape: const StadiumBorder(
              side: BorderSide(
                color: Color(0xff000000),
                width: 0,
                style: BorderStyle.none,
              ),
            ),
          ),
          dialogTheme: const DialogTheme(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          cardTheme: CardTheme(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
           */
        );
    }
  }
}
