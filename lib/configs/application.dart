import 'package:shared_preferences/shared_preferences.dart';

class Application {
  static const String appName = 'Flutter';
  static const bool debug = true;
  static const String version = '1.0.0';
  static SharedPreferences? prefs;

  ///Singleton factory
  static final Application _instance = Application._internal();

  factory Application() {
    return _instance;
  }

  Application._internal();
}
