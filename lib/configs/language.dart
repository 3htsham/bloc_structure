import 'package:flutter/material.dart';

class AppLanguage {
  ///Default Language
  static Locale defaultLanguage = const Locale('en');

  ///Supported Languages
  static List<Locale> supportedLocales = [
    const Locale('en'),
    const Locale('ar'),
    const Locale('fr')
  ];

  ///Singleton factory
  static final AppLanguage _instance = AppLanguage._internal();

  factory AppLanguage() {
    return _instance;
  }

  AppLanguage._internal();
}
