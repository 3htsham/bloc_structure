import 'package:flutter/material.dart';

import '../enums/enums.dart';
import '../models/models.dart';
import 'configs.dart';

class AppTheme {
  static const String defaultFont = 'Raleway';

  ///Default font
  static String currentFont = 'Raleway';

  ///List Font support
  static List<String> fontSupport = ['Raleway'];

  ///Default Theme
  static ThemeModel currentTheme = ThemeModel.fromJson({
    'name': 'default',
    'color': const Color(0xff0093e6),
    'light': 'primaryLight',
    'dark': 'primaryDark',
  });

  ///List Theme Support in Application
  static List<ThemeModel> themeSupport = [
    {
      'name': 'default',
      'color': const Color(0xff0093e6),
      'light': 'primaryLight',
      'dark': 'primaryDark',
    },
  ].map((item) => ThemeModel.fromJson(item)).toList();

  ///Dark Theme option
  static DarkOption darkThemeOption = DarkOption.alwaysOff;
  static ThemeData lightTheme =
      CollectionTheme.getCollectionTheme(theme: currentTheme.lightTheme);
  static ThemeData darkTheme =
      CollectionTheme.getCollectionTheme(theme: currentTheme.darkTheme);

  ///Singleton factory
  static final AppTheme _instance = AppTheme._internal();

  factory AppTheme() {
    return _instance;
  }

  AppTheme._internal();
}
