import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';
import 'utils/utils.dart';

class AppObserver extends BlocObserver {
  @override
  void onCreate(BlocBase bloc) {
    super.onCreate(bloc);
    UtilLogger.log('BLOC CREATED', bloc);
  }

  @override
  void onClose(BlocBase bloc) {
    super.onClose(bloc);
    UtilLogger.log('BLOC CLOSED', bloc);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    UtilLogger.log('BLOC ERROR', error);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    UtilLogger.log('BLOC TRANSITION', transition);
  }

  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    UtilLogger.log('BLOC CHANGE', change);
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    UtilLogger.log('BLOC EVENT', event);
  }
}

void main() {
  BlocOverrides.runZoned(() => runApp(const App()),
      blocObserver: AppObserver());
}
