import '../configs/configs.dart';

class UtilPreferences {
  static bool containsKey(String key) {
    return Application.prefs!.containsKey(key);
  }

  static Future<bool> setString(String key, String value) async {
    return Application.prefs!.setString(key, value);
  }

  static String? getString(String key) {
    return Application.prefs!.getString(key);
  }

  static Future<bool> setBool(String key, bool value) async {
    return Application.prefs!.setBool(key, value);
  }

  static bool getBool(String key) {
    return Application.prefs!.getBool(key) ?? false;
  }
}
